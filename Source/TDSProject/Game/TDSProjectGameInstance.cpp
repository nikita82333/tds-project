// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSProject/Game/TDSProjectGameInstance.h"

bool UTDSProjectGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFound = false;
	FWeaponInfo* WeaponInfoRow;

	if (WeaponInfoTable)
	{
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFound = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTDSProjectGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	return bIsFound;
}
